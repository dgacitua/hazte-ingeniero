var xmlhttp;
 if (window.XMLHttpRequest)
   {// code for IE7+, Firefox, Chrome, Opera, Safari
   xmlhttp=new XMLHttpRequest();
   }
 else
   {// code for IE6, IE5
   xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
   }


function ajax_showhidetoggle(id,estadoactual){
	

			xmlhttp.onreadystatechange=function()  {
				
		   if (xmlhttp.readyState==4 && xmlhttp.status==200)    {
			   	   if(xmlhttp.responseText != "1") { 
				  		 alert_msg("Error desconocido");
				   } else {				   
						 window.location.reload();
				   }
			 }
		   }
		   
		 xmlhttp.open("POST", ajax_url, true);
		 xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		 xmlhttp.send("action=showhidetoggle&id="+id+"&estadoactual="+estadoactual);
		
		
}





function ajax_deletefile(id){
	
if(confirm("Borrar archivo?")) {
			xmlhttp.onreadystatechange=function()  {
		   if (xmlhttp.readyState==4 && xmlhttp.status==200)    {
			  
			   	   if(xmlhttp.responseText != "1") { 
				  		 alert_msg("Error desconocido"); 
				   } else {				   
						 window.location.reload();
				   }
			 }
		   }
		   
		 xmlhttp.open("POST", ajax_url, true);
		 xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		 xmlhttp.send("action=deleteFile&id="+id);
	} // confirmacion
}




function ajax_deletefolder(id){
	
	
	if(confirm("Borrar carpeta?")) {
			xmlhttp.onreadystatechange=function()  {
		   if (xmlhttp.readyState==4 && xmlhttp.status==200)    {
			 
			   	   if(xmlhttp.responseText != "1") { 
				  		 alert_msg("Error desconocido"); 
				   } else {				   
						 window.location.reload();
				   }
			 }
		   }
		   
		 xmlhttp.open("POST", ajax_url, true);
		 xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		 xmlhttp.send("action=deleteFolder&id="+id);
	} // confirmacion
}

function alert_msg(str){
	document.getElementById("error_message").innerHTML = str;
	document.getElementById("error_message").style.backgroundColor="#ffaaaa"; // al peo este cambio de color
	}

function ajax_create_new_folder(){
	
	var nombre_carpeta = document.getElementById("new_folder_name").value;
	var slug_carpeta = document.getElementById("new_folder_slug").value.toLowerCase();
	var desc_carpeta = document.getElementById("new_folder_desc").value;
	
	if(slug_carpeta == "" || nombre_carpeta == ""){		
		alert_msg("Error al crear carpeta: Falta llenar campos mijito");		
		}
	
	else if(!slug_carpeta.match(/^[A-Za-z0-9-]+$/g)){		
		alert_msg("Error al crear carpeta: Slug solamente acepta a-z A-Z 0-9 y guion");
		}
	
	
	else{
				xmlhttp.onreadystatechange=function()  {
		   if (xmlhttp.readyState==4 && xmlhttp.status==200)    {
			 
			   	   if(xmlhttp.responseText != "1") { 
				  		 alert_msg(xmlhttp.responseText); 
				   } else {				   
						 window.location.reload();
				   }
			 }
		   }
		   
		   
		 xmlhttp.open("POST", ajax_url, true);
		 xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		 xmlhttp.send("action=createFolder&id="+id_carpeta_actual+"&nombre="+encodeURIComponent(nombre_carpeta)+"&slug="+encodeURIComponent(slug_carpeta)+"&desc="+encodeURIComponent(desc_carpeta));
	}
}


function is_valid_url(url) {
     return url.match(/^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/);
}


function ajax_create_new_file(){
	
	var nombre_file = document.getElementById("new_file_name").value;
	var desc_file = document.getElementById("new_file_desc").value;
	var l1_file = document.getElementById("new_file_link1").value;
	var l2_file = document.getElementById("new_file_link2").value;
	var l3_file = document.getElementById("new_file_link3").value;
	var public;
	if(document.getElementById("new_file_public").checked) {
		public = 1;
		}
		else {
			public = 0;
			}
	
	if(nombre_file == ""){		
		alert_msg("Error al crear archivo: Debe tener nombre");		
		}
	else if(l1_file == "" && l2_file == "" && l3_file == ""){
		alert_msg("Error al crear archivo: Debe tener al menos un link");
		
		}
	

	else{
		 

		
		if(!is_valid_url(l1_file) && !is_valid_url(l2_file) && !is_valid_url(l3_file)) {
			
				if(!confirm("No se encontraron URLs (deben empezar con HTTP, ftp, https, etc. Desea continuar de todos modos?)")) {
					return false;
					}
				
			 } // no se detecto URL
		
				xmlhttp.onreadystatechange=function()  {
		   if (xmlhttp.readyState==4 && xmlhttp.status==200)    {
			 
			   	   if(xmlhttp.responseText != "1") { 
				  		 alert_msg(xmlhttp.responseText); 
				   } else {				   
						 window.location.reload();
				   }
			 }
		   }
		  
		   
		   
		 xmlhttp.open("POST", ajax_url, true);
		 xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		 xmlhttp.send("action=createFile&id="+id_carpeta_actual+"&nombre="+encodeURIComponent(nombre_file)+"&desc="+encodeURIComponent(desc_file)+"&l1="+encodeURIComponent(l1_file)+"&l2="+encodeURIComponent(l2_file)+"&l3="+encodeURIComponent(l3_file)+"&public="+encodeURIComponent(public));
		
	}
}