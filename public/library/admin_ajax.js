var submit_btn;


function abrir_editor_carpeta(){
	
	document.getElementById('new_folder_form').style.display='';
	document.getElementById('new_file_form').style.display='none';
	cancelar_edicion()
	}

function abrir_editor_archivo(){
	
	document.getElementById('new_file_form').style.display='';
	document.getElementById('new_folder_form').style.display='none';
	cancelar_edicion()
	}
	

function post_data(params) {
    
    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "");

	block_button();

for(i=0 ; i<Object.keys(params).length ; i++) {
	
	        var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", Object.keys(params)[i]);
            hiddenField.setAttribute("value", params[Object.keys(params)[i]]);

            form.appendChild(hiddenField);

	
	
	}

    document.body.appendChild(form);
    form.submit();
}



/*

function ajax_execute_hing(send_string){


post();





	/*
var xmlhttp;
 if (window.XMLHttpRequest)
   {// code for IE7+, Firefox, Chrome, Opera, Safari
   xmlhttp=new XMLHttpRequest();
   }
 else
   {// code for IE6, IE5
   xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
   }
	
	
			xmlhttp.onreadystatechange=function()  {
				
		   if (xmlhttp.readyState==4 && xmlhttp.status==200)    {
			   
			   document.getElementById("loading_gif").style.display="none";
			   
			   if(xmlhttp.responseText == "1") {				  
				   window.location.reload();
				   } else {
					   
					   unblock_button()
					   // si hubo algun error
					   
					    if(xmlhttp.responseText != ""){
					   alert_msg(xmlhttp.responseText); 					   
					   } else {
						   alert_msg("Error desconocido");
						   }
					   				   
					   
					   
					   } 
			 }
		   }
		   
		 block_button();
		 document.getElementById("loading_gif").style.display="";
		 
		 xmlhttp.open("POST", ajax_url, true);
		 xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		 xmlhttp.send(send_string);

*/


/*

$.ajax({beforeSend: function(){ 
				block_button();			
				document.getElementById("loading_gif").style.display="";
		  },
		type: "POST", 
		data: send_string, 
		url:ajax_url, 
		success:function(result){
				sending_ajax = false;
				if(result == "1") {				  
				   window.location.reload();
				   } else {
					   document.getElementById("loading_gif").style.display="none";
					   unblock_button()
					   // si hubo algun error
					   
					    if(result != ""){
					   alert_msg(result); 					   
					   } else {
						   alert_msg("Error desconocido");
						   }
				   }
						
				
								
		  }});
*/




		
	
	//}


		  

	


function block_button(){	
		if(submit_btn){
			document.getElementById(submit_btn).disabled = true;	
		}
	}
	
function unblock_button(){	
		if(submit_btn){
			document.getElementById(submit_btn).disabled = false;
		}
	}


function alert_msg(str){
	document.getElementById("error_message").innerHTML = str;
	}
	
	
function is_valid_url(url) {     
	 if(url.search("http://")==0 || url.search("https://")==0 || url.search("ftp://")==0 || url=="") { return true; }	 
	 return false; 	 
}
	
	// funciones de modificaciones


function ajax_showhidetoggle(id,estadoactual){
	

	params = {  action : 'showhidetoggle', 
				id : id, 
				estadoactual : estadoactual
			}


	post_data(params);
					
					 //ajax_execute_hing("action=showhidetoggle&id="+id+"&estadoactual="+estadoactual);		
		
}


function ajax_showhidetoggleFOLDER(id,estadoactual){
	
	
		params = {  action : 'showhidetoggleFOLDER', 
					id : id, 
					estadoactual : estadoactual
			}


	post_data(params);
	
					// ajax_execute_hing("action=showhidetoggleFOLDER&id="+id+"&estadoactual="+estadoactual);		
		
}



function ajax_deletefile(id){
	
if(confirm("Borrar archivo?")) {


			params = {  action : 'deleteFile', 
					id : id
			}


	post_data(params);
 
		 
	} // confirmacion
}




function ajax_deletefolder(id){
	
		if(confirm("Borrar carpeta?")) {	


		params = {  action : 'deleteFolder', 
					id : id 
				}


	post_data(params);
		 
	} // confirmacion
}




function ajax_create_new_folder(){
	
	
	
	var nombre_carpeta = document.getElementById("new_folder_name").value;
	var slug_carpeta = document.getElementById("new_folder_slug").value.toLowerCase();
	var desc_carpeta = document.getElementById("new_folder_desc").value;
	var editing_folder1 = document.getElementById("editing_folder").value;
	if(document.getElementById("new_folder_public").checked) {
		public = 1;
		}
		else {
			public = 0;
			}
	
	if(slug_carpeta == "" || nombre_carpeta == ""){		
		alert_msg("Error al crear carpeta: Falta llenar los campos necesarios (marcados con *)");		
		}
	
	else if(!slug_carpeta.match(/^[A-Za-z0-9-]+$/g)){		
		alert_msg("Error al crear carpeta: Slug solamente acepta caracteres a-z A-Z 0-9 y guión");
		}
	
	
	else{
		
		
				params = {  action : 'createFolder', 
					id : id_carpeta_actual, 
					nombre : (nombre_carpeta),
					slug : (slug_carpeta),
					desc : (desc_carpeta),
					public : (public),
					editing_folder : editing_folder1
				}


	post_data(params);
		

	}
}





function ajax_create_new_file(){
	
	var nombre_file = document.getElementById("new_file_name").value;
	var desc_file = document.getElementById("new_file_desc").value;
	var l1_file = document.getElementById("new_file_link1").value;
	var l2_file = document.getElementById("new_file_link2").value;
	var l3_file = document.getElementById("new_file_link3").value;
	var editing_file1 = document.getElementById("editing_file").value;
	var public;
	if(document.getElementById("new_file_public").checked) {
		public = 1;
		}
		else {
			public = 0;
			}
	
	if(nombre_file == ""){		
		alert_msg("Error al crear archivo: Debe tener un nombre");	
			
		} else  if(l1_file.length>maxlength_links || l2_file.length>maxlength_links || l3_file.length>maxlength_links){
				alert_msg("Error al crear archivo: Los links deben tener un maximo de "+maxlength_links+" caracteres");
					
			}
	else if(l1_file == "" && l2_file == "" && l3_file == ""){
		alert_msg("Error al crear archivo: Debe tener al menos un mirror (link)");
		
		}
	

	else if(!is_valid_url(l1_file) || !is_valid_url(l2_file) || !is_valid_url(l3_file)) {
			
				alert_msg("Error al crear archivo: La URL debe comenzar con http:// https:// ftp://");
				
			 } // no se detecto URL
		
		else {
		
		
			params = {  action : 'createFile', 
					id : id_carpeta_actual, 
					nombre : (nombre_file),
					desc : (desc_file),
					public : (public),
					l1 : (l1_file),
					l2 : (l2_file),
					l3 : (l3_file),
					editing_file : editing_file1
				}


	post_data(params);
		}
			
		
	
}
