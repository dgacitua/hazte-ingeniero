function begin_editing_folder(id){
	
		cancelar_edicion()
		abrir_editor_carpeta();
		
		$("#new_folder_name").val($("#editfolder_name"+id).val());
		$("#new_folder_slug").val($("#editfolder_slug"+id).val());
		$("#new_folder_desc").val($("#editfolder_desc"+id).val());
		
		if($("#editfolder_public"+id).val() == 1){
				$("#new_folder_public").prop("checked", true);
			} else {
				$("#new_folder_public").prop("checked", false);
				}
	
		
		$("#editing_folder").val(id);	
		$("#editing_btn").show();
		
		$(".admin_little_panel").get(0).scrollIntoView();
	
	}


function begin_editing_file(id){
	
		cancelar_edicion()
		abrir_editor_archivo();
		
		$("#new_file_name").val($("#editfile_name"+id).val());
		$("#new_file_desc").val($("#editfile_desc"+id).val());
		$("#new_file_link1").val($("#editfile_l1_"+id).val());
		$("#new_file_link2").val($("#editfile_l2_"+id).val());
		$("#new_file_link3").val($("#editfile_l3_"+id).val());
		
		if($("#editfile_public"+id).val() == 1){
				$("#new_file_public").prop("checked", true);
			} else {
				$("#new_file_public").prop("checked", false);
				}
	
		
		$("#editing_file").val(id);	
		$("#editing_btn").show();
		
		$(".admin_little_panel").get(0).scrollIntoView();
	
	}	


function cancelar_edicion(){
	
		$("#editing_btn").hide(); // esconder el boton
		$("#editing_folder").val(""); // hidden
		$("#editing_file").val(""); // hidden
		
		$("#new_folder_public").prop("checked", true);
		$("#new_file_public").prop("checked", true);
		
		$("[name='create_folder_form']").find("input[type='text']").val("");
		$("[name='create_file_form']").find("input[type='text']").val("");
	
	}