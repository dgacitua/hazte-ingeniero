<?php 


function get_title(){

	return "Hazte Ingeniero USACH"; 
	
	
	}



function get_header($params){
	
	// parametros que se deben pasar a la funcion
		// title, titulo de la pestana
		// nombre_pag, como se llama la pagina, titulo grande (Hazte Ingeniero, Acerca)
		// sub_pag subtitulo de la pagina (lo que va abajo del titulo grande)
		// url_banner url del banner, ejemplo 'img/bear.jpg'
	
	?><!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $GLOBALS['descripcion_meta']; ?>">
    <meta name="author" content="<?php echo $GLOBALS['autor_meta']; ?>">

    <title><?php echo $params["title"]; ?></title>
    
    <!-- jQuery -->
    <script src="<?php echo URL::to('/'); ?>/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo URL::to('/'); ?>/js/bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo URL::to('/'); ?>/js/clean-blog.min.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo URL::to('/'); ?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo URL::to('/'); ?>/css/clean-blog.min.css" rel="stylesheet">

    <!-- Carpetas Lista -->
    <link href="<?php echo URL::to('/'); ?>/css/carpetaslist.css" rel="stylesheet">
    
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo URL::to('/favicon.ico'); ?>" type="image/x-icon">
	<link rel="icon" href="<?php echo URL::to('/favicon.ico'); ?>" type="image/x-icon">

    <!-- Custom Fonts -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Alternar Navegación</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo URL::to('/'); ?>">Hazte Ingeniero USACH</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?php echo URL::to('/'); ?>">Página Principal</a>
                    </li>
                    <li>
                        <a href="<?php echo URL::to('/acerca'); ?>">Acerca de</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('<?php echo URL::to('/')."/".$params["url_banner"]; ?>')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1><?php echo $params["nombre_pag"]; ?></h1>
                        <hr class="small">
                        <span class="subheading"><?php echo $params["sub_pag"]; ?></span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container" id="mainContainer">
    
	<?php
	}




function get_footer(){
	?>
	</div>
    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <ul class="list-inline text-center">
                        <li>
                            <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-github fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                    <p class="copyright text-muted">Copyright &copy; Hazte Ingeniero USACH 2014</p>
                </div>
            </div>
            <p class="copyright text-muted"><?php
                if (Auth::user()) {
                    echo "<a href='/user'>Panel Usuario</a> - <a href='/admin'>Panel Admin</a> - <a href='/user/logout'>Logout</a>";
                }
                else {
                    echo "<a href='/user'>Login Uploaders</a>";
                }
                ?>
            </p>
        </div>
    </footer>
</body>
</html>

	
	
	<?php
	}









?>