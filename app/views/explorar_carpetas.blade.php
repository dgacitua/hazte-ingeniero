<?php get_header(array('title' => 'Hazte Ingeniero USACH',
						'nombre_pag' => 'Hazte Ingeniero USACH',
						'sub_pag' => 'Apuntes para estudiantes de Ingeniería USACH',
						'url_banner' => 'img/home-bg.jpg'
						));
 ?>



<?php 

// includes

if($GLOBALS["logged"]) {
	require_once(app_path().'/views/CarpetasViewLib/print_botones_admin.php');
	require_once(app_path().'/views/CarpetasViewLib/get_javascript_code.php'); 
	
}

require_once(app_path().'/views/CarpetasViewLib/funciones.php'); 




?>




<?php

	echo '<div id="lista">';

// obtener menu de navegacion
$navigation_menu = get_navigation($clean_url);

if($navigation_menu != ""){
	
		echo '<h4>'.$navigation_menu.'</h4>';
	
	} else {
		
		echo '<p>Te encuentras en la Página Principal</p>';
		
		}
		
			
 ?>




<?php 

// includes

if($GLOBALS["logged"]) {
	require(app_path().'/views/CarpetasViewLib/crear_carpeta_archivo.php'); // vista de admin
	
}

?>



<?php



if(hayArchivos($files) || hayCarpetas($subfolders)) {
	
	
	
	if(hayCarpetas($subfolders)) { // hay subcarpetas
		
		echo "<h2>Carpetas</h2>";
		
		
		  foreach ($subfolders as $sf) {		 
			  if($GLOBALS["logged"] || $sf->carpeta_publica==1){ 
			  
			  			if($clean_url == "") { // OBTENER LA URL HACIA ESTA FOLDER
							  $folder_url = URL::to('/')."/".$sf->carpeta_slug;
							  } else {
								  $folder_url = URL::to('/')."/".$clean_url."/".$sf->carpeta_slug;
								  }
			  
			  
						  echo "<div id='carpetas' class='carpetalist_folder'>";	
							
						  if($GLOBALS["logged"]){
							  echo "<div class='carpeta_list_edit_buttons'>";
							  editFolder($sf->id_carpeta);
							  publicPrivateCARPETA($sf->id_carpeta, $sf->carpeta_publica);
							  deleteFolder($sf->id_carpeta);							  
							  echo "</div>";
						  }
						  
					
						
						  echo "<span class='list_content'><a href='".$folder_url."'>".print_from_db($sf->nombre_carpeta)."</a></span>";	
						  if($GLOBALS["logged"]){ printEditFolderForm($sf->id_carpeta, $sf->descripcion_carpeta, $sf->nombre_carpeta, $sf->carpeta_slug, $sf->carpeta_publica); }			  
						  echo "</div>";		
					 }
			  }
		
		
        		
		
		
		} // if hay subcarpetas
	
	
	if(hayArchivos($files)) { // hay archivos
		
		
		echo "<h2>Archivos</h2>";
		
		
		        foreach ($files as $fl){
					
					if($GLOBALS["logged"] || $fl->archivo_publico==1){	
						echo "<div id='archivos' class='carpetalist_file'>";					
						//echo HTML::image("images/file_icon.jpg", "");
						
						
						if($GLOBALS["logged"]){
							echo "<div class='carpeta_list_edit_buttons'>";
							editFile($fl->id_archivo);
							publicPrivate($fl->id_archivo, $fl->archivo_publico);
							deleteFile($fl->id_archivo);							
							echo "</div>";
						}
						
						echo "<span class='list_content'><a href=".print_from_db($fl->link1).">";
						echo print_from_db($fl->nombre_archivo);
						echo "</a>";
						if($fl->link2) { echo "<a class='mirrorlink' href='".$fl->link2."'>(mirror #1)</a>";}
						if($fl->link3) { echo "<a class='mirrorlink' href='".$fl->link3."'>(mirror #2)</a>";}
						echo "</span>";
						if($GLOBALS["logged"]){ printEditFileForm($fl->id_archivo, $fl->descripcion_archivo, $fl->nombre_archivo, $fl->archivo_publico, $fl->link1, $fl->link2, $fl->link3); }	
						echo "</div>";					
					}
				}
        	
             
		
		
		} // if hay archivos
	
	
	
	} else { // no hay ni uno ni lo otro
		
		
		echo "<p><i>Carpeta vacía</i></p>";
		
		
		}

	echo '</div>';

?>
<hr>
<h2>¡Bienvenido a Hazte Ingeniero!</h2>
<p>Este sitio es un repositorio de apuntes para el Módulo Básico de Ingeniería USACH. Totalmente público y gratuito. Creado por estudiantes, para estudiantes.</p>
<p>Puedes dejar tus sugerencias y aportes a nuestro correo: hazteingeniero@gmail.com</p>
<?php
// se genera el codigo jQuery para scroll automatico

if($id_carpeta_actual != null){	
	echo    "<script>
			$(document).ready(function(){	
			$('html,body').animate({
			  	scrollTop: $('#mainContainer').offset().top
			}, 500);

			})
		</script>";	
	} 
	// ELSE: se encuentra en la homepage y no debe haber scroll automatico
?>
<?php get_footer(); ?>