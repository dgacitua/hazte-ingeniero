<?php 

function hayArchivos(&$arr){ 	
	if($GLOBALS["logged"] && sizeof($arr)>0) { return true; }
	
	for($i=0; $i<sizeof($arr); $i++){		
		if($arr[$i]->archivo_publico == 1){ return true; }	
		}		
	return false;	
	}


function hayCarpetas(&$arr){ 	
	if($GLOBALS["logged"] && sizeof($arr)>0) { return true; }
	
	for($i=0; $i<sizeof($arr); $i++){		
		if($arr[$i]->carpeta_publica == 1){ return true; }	
		}		
	return false;	
	}


function get_navigation($clean_url){
	
	// Entrada: la URL (string)
	// Salida: retorna todo el codigo HTML que se pone en el navegador (string)
	
	
	if($clean_url != "") {
		$return = "<a href='".URL::to('/')."'><input type='button' class='homepage_btn' value=' '></a>";
		
		$pedazos = explode("/", $clean_url);
		for($i=0; $i < sizeof($pedazos); $i++){
			
			
			$link = "";
			for($w=0;$w<=$i;$w++){
				$link .= "/".$pedazos[$w];
				}

			
			if($i < sizeof($pedazos)-1){
				$return.="　→　<a href='".URL::to('/').$link."'>".print_from_db(DB::table('carpeta')->where('carpeta_slug', $pedazos[$i])->pluck('nombre_carpeta'))."</a>";
			} 
			else {				
				$return.="　→　<b>".print_from_db(DB::table('carpeta')->where('carpeta_slug', $pedazos[$i])->pluck('nombre_carpeta'))."</b>";
				}
			
			}
		
		
		return $return;
		}	
	return false;	
	} 
	
	


?>