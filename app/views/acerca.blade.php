<?php get_header(array('title' => 'Hazte Ingeniero USACH',
						'nombre_pag' => 'Acerca del Proyecto',
						'sub_pag' => 'Hazte Ingeniero es un proyecto de Team Clippy',
						'url_banner' => 'img/home-bg.jpg'
						));
 ?>


<p>"Hazte Ingeniero" es una iniciativa creada por el grupo de emprendimiento "Team Clippy", como proyecto formativo para la asignatura de Evaluación y Gestión de Proyectos 2º Semestre 2014 de Ingeniería Informática USACH.</p>
<p>La idea de este proyecto nace en la poca acogida y el abandono que han tenido los estudiantes del Módulo Básico de Ingeniería del Plan 2012 en lo que respecta a la recolección de apuntes, ejercicios, guías y pruebas para fomentar sus estudios. "Hazte Ingeniero" nace como respuesta a esta incipiente necesidad ofreciendo material de estudio con el objetivo de "nivelar hacia arriba" los conocimientos de los futuros Ingenieros USACH.</p>
<p>Team Clippy está formado por:</p>
<ul>
	<li>Daniel Brown Madariaga
	<li>Víctor Cañete Figueroa
	<li>Javier Castro Berríos
	<li>Fabián Escobar Orellana
	<li>Gerardo Fernández Hormazabal
	<li>Daniel Gacitúa Vásquez
	<li>Angus Pollmann Stocker
	<li>David Vásquez Melo
	<li>Felipe Vilches Céspedes
</ul>

Si quieres contactar a la Administración del Sitio, puedes hacerlo a hazteingeniero@gmail.com

<?php get_footer(); ?>