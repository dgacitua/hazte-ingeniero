<?php


class CarpetasController extends BaseController {



	public function getCarpetas($url_slug) 	{ // funcion llamada del router
	
		
		$model = new Carpetas;
	
		$clean_url = $model->procesar_url_obtenida($url_slug); // el modelo procesa la URL y saca de la base de datos las carpetas necesarias
	
		$id_carpeta_actual = $model->obtener_id_carpeta_actual($clean_url);	// retorna id de carpeta actual (en la que se esta navegando)
		
	
		// Datos a mostrar (carpetas y archivos)
		$subfolders = $model->get_subfolders($id_carpeta_actual);
		$files = $model->get_files($id_carpeta_actual);
	
		// Crear la vista y pasar las variables
		
		
				
		return View::make('explorar_carpetas', compact('subfolders', 'files', 'clean_url', 'id_carpeta_actual')); 
		
			
		
		
	}
}