<?php

 


class Carpetas {
 

// configuracion de usuario loggeado



static public function procesar_url_obtenida($url_slug){ 

// Entrada: URL a la que se quiere entrar.
// Salida: La misma URL pero limpia, sin espacios, ni ningun caracter raro. Si el sitio se usa normalmente sin meter mano, la entrada es igual que la salida.
	
	$url_slug = str_replace(" ","",$url_slug);
	$url_slug = str_replace("\t","",$url_slug);
	while(strpos($url_slug, "//") > -1) { $url_slug = str_replace("//", "/", $url_slug); } // eliminar slash duplicados si es que hay (varios slash juntos)
	if(substr($url_slug,0,1) == "/") { $url_slug = substr($url_slug,1, strlen($url_slug)); } // eliminar los primeros / en exceso
	if(substr($url_slug,strlen($url_slug)-1,1) == "/") { $url_slug = substr($url_slug,0, strlen($url_slug)-1); } // eliminar los ultimos / en exceso
	
	return $url_slug;		
	
	}


static public function obtener_id_carpeta_actual($clean_url){
	
	// Entrada: URL
	// Salida: id de la carpeta actual (en la que estamos navegando)
	
	if($clean_url != "") {		
				
			
						$pedazos = explode("/", $clean_url);						
						$id = null;
						
					for($i=0 ; $i < sizeof($pedazos); $i++){ // ITERACION QUIZAS INEFICIENTE, HABRA ALGUNA FORMA DE HACERLO CON UN PURO QUERY??
					
						if($GLOBALS["logged"]){
							$id = DB::table('carpeta')->where('carpeta_slug', $pedazos[$i])->where('carpeta_padre', $id)->pluck('id_carpeta');
							}else {
								$id = DB::table('carpeta')->where('carpeta_slug', $pedazos[$i])->where('carpeta_padre', $id)->where('carpeta_publica',1)->pluck('id_carpeta');
								}					
						
						if($id == null) { 
						
							App::abort(404);
						}
						// abortar en caso de que no se encuentre (error 404)
					}
			
			return $id;
	} 
	
	else {
	return null;
	}
	
}




static public function get_subfolders($id){
	// Entrada: id de carpeta
	// Salida: Lo que se encontro en la base de datos (array de resultados de subcarpetas)	
	return DB::table('carpeta')->where('carpeta_padre', $id)->orderBy('nombre_carpeta', 'asc')->get();
	} 
	
	
static public function get_files($id){
	// Entrada: id de carpeta
	// Salida: Lo que se encontro en la base de datos (array de resultados de archivos)
	return DB::table('archivo')->where('archivo_padre', $id)->orderBy('nombre_archivo', 'asc')->get();
	} 


}



?>