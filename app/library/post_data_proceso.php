<?php 


 function post_success(){
	 if($_POST["action"] == "createFolder" || $_POST["action"] == "createFile") { $_SESSION["last_post"] = time(); }
		header('Location: '.Request::url());
		 die();
	 }
 
 function post_error($msg = ""){
	 	$_SESSION['error_mensaje_post_data'] = $msg; 		
		 header('Location: '.Request::url());
		 die();
		
		
	 }

if($_POST["action"] == "createFolder" || $_POST["action"] == "createFile") {
			
			if(isset($_SESSION["last_post"])) {	
				if(abs(time()-$_SESSION["last_post"]) < 5) {
					post_error("ERROR: Solo se puede crear carpetas y archivos cada 5 segundos para evitar sobrecarga en el servidor");		
					}	
				}

		}
		
		
 
// mostrar & ocultar archivo
	
			if($_POST["action"]=="showhidetoggle" && valid_number($_POST["id"]) && valid_number($_POST["estadoactual"]) && $GLOBALS['editar']){	
			
					if($_POST["estadoactual"] == 1){ $nuevo_estado = 0;}
					else { $nuevo_estado = 1; }
					
					DB::update('update archivo set archivo_publico = ? where id_archivo = ? ', array($nuevo_estado, $_POST["id"]));
		
					post_success();
				}
	
	
	
// mostrar & ocultar carpeta
	
			if($_POST["action"]=="showhidetoggleFOLDER" && valid_number($_POST["id"]) && valid_number($_POST["estadoactual"]) && $GLOBALS['editar']){	
			
					if($_POST["estadoactual"] == 1){ $nuevo_estado = 0;}
					else { $nuevo_estado = 1; }
					
					DB::update('update carpeta set carpeta_publica = ? where id_carpeta = ? ', array($nuevo_estado, $_POST["id"]));
		
					post_success();	
				}
				
				
	
	// borrar archivo
	
	
			else if($_POST["action"]=="deleteFile" && valid_number($_POST["id"]) && $GLOBALS['borrar']){	
					DB::table('archivo')->where('id_archivo', '=', $_POST["id"])->delete();			
					post_success();
			}
	
	
	// borrar carpeta
	
	
			else if($_POST["action"]=="deleteFolder" && valid_number($_POST["id"]) && $GLOBALS['borrar']){	
					
					DB::table('carpeta')->where('id_carpeta', $_POST["id"])->delete();
					// borra todos los "child".. archivos y carpetas					
					post_success();
			} 
			
			
	// crear carpeta
			
			else if($_POST["action"]=="createFolder" && valid_textfield($_POST["nombre"]) && isset($_POST["desc"]) && isset($_POST["editing_folder"]) && valid_slug($_POST["slug"]) && valid_number($_POST["id"]) && valid_number($_POST["public"]) && $GLOBALS['crear']){
					
				$nombre = substr($_POST["nombre"],0,$GLOBALS["maxlength_ambas_nombre"]); // max length
				$slug = strtolower(substr($_POST["slug"],0,$GLOBALS["maxlength_carpeta_slug"])); // max length
				$desc = nullify(substr($_POST["desc"],0,$GLOBALS["maxlength_ambas_desc"])); // max length
				$id = $_POST["id"];
				
				// se esta editando o creando nueva carpeta
				
				$editing = false;
				if($_POST["editing_folder"] == "") {
						// se esta creando una carpeta nueva
						$editing = false;
					} elseif($_POST["editing_folder"]){
						// se esta editando
						$editing = true;						
						}
				
				
			
				if($id == 0 ) { $id = null ; }
				
				if($_POST["public"] == 1) {
					$estado_publico = 1;
					} else {
						$estado_publico = 0;
						}
				
				
				
				
				if($editing == false){	// se crea una nueva carpeta	
		
					if($id){
					$count = DB::select( DB::raw("SELECT count(*) as a FROM carpeta WHERE carpeta_padre = ? AND (nombre_carpeta=? OR carpeta_slug=?)"), array($id, $nombre, $slug));
					} else {
						$count = DB::select( DB::raw("SELECT count(*) as a FROM carpeta WHERE carpeta_padre IS NULL AND (nombre_carpeta=? OR carpeta_slug=?)"), array($nombre, $slug));
						}
					
					
					if($count[0]->a > 0){					
						post_error("ERROR: La carpeta creada ya existe");
						}
							
					DB::table('carpeta')->insert(array('nombre_carpeta' => $nombre, 
														'carpeta_slug' => $slug, 
														'descripcion_carpeta' => $desc, 
														'carpeta_padre' => $id, 
														'carpeta_publica' => $estado_publico));
					
				} else if($editing) { // SE EDIta 
					
					
										DB::table('carpeta')
           			 ->where('id_carpeta', $_POST['editing_folder'])
            		 ->update(array('nombre_carpeta' => $nombre, 
														'carpeta_slug' => $slug, 
														'descripcion_carpeta' => $desc, 
														'carpeta_padre' => $id, 
														'carpeta_publica' => $estado_publico));
					
					
					
					
					}
				
				
				
				
				
				post_success();
			}
			
			
			// crear archivo
			
			
				else if($_POST["action"]=="createFile" && valid_textfield($_POST["nombre"]) && isset($_POST["desc"]) && isset($_POST["editing_file"]) && (valid_textfield($_POST["l1"]) || valid_textfield($_POST["l2"]) || valid_textfield($_POST["l3"])) && valid_number($_POST["id"]) && valid_number($_POST["public"]) && $GLOBALS['crear']){
					
				$nombre = substr($_POST["nombre"],0,$GLOBALS["maxlength_ambas_nombre"]); // max length
				$desc = nullify(substr($_POST["desc"],0,$GLOBALS["maxlength_ambas_desc"])); // max length
				$id = $_POST["id"];
				
				
				
				// se esta editando o creando nuevo archivo
				
				$editing = false;
				if($_POST["editing_file"] == "") {
						// se esta creando una carpeta nueva
						$editing = false;
					} elseif($_POST["editing_file"]){
						// se esta editando
						$editing = true;						
						}
				
				
				
				if($id == 0) { $id = null ; }
				
				if($_POST["public"] == 1) {
					$estado_publico = 1;
					} else {
						$estado_publico = 0;
						}
				
				
				$links = array();
				
				for($i=1; $i<=3 ; $i++){	
					if($_POST["l".$i]){
						$links[] = substr($_POST["l".$i],0,$GLOBALS["maxlength_archivo_link"]);
					}
				}
				
				if(!isset($links[1])) { $links[1]=null; }
				if(!isset($links[2])) { $links[2]=null; }
					
		
				if($editing == false){	// se crea una nueva carpeta
		
				if($id) { 
				$count = DB::select( DB::raw("SELECT count(*) as a FROM archivo WHERE archivo_padre=? AND nombre_archivo=?"), array($id, $nombre));
				} else {				
				
				$count = DB::select( DB::raw("SELECT count(*) as a FROM archivo WHERE archivo_padre is null AND nombre_archivo=?"), array($nombre));
				}
				
											
				if($count[0]->a > 0){									
					post_error("ERROR: Ya existe un archivo con ese nombre");
					}
								

				DB::table('archivo')->insert(array('nombre_archivo' => $nombre, 
													'descripcion_archivo' => $desc, 
													'archivo_padre' => $id, 
													'archivo_publico' => $estado_publico,
													'link1' => $links[0],
													'link2' => $links[1],
													'link3' => $links[2]));
													
				} elseif($editing){ // se esta editando
					
					
					
					DB::table('archivo')
           			 ->where('id_archivo', $_POST['editing_file'])
            		 ->update(array('nombre_archivo' => $nombre, 
													'descripcion_archivo' => $desc, 
													'archivo_padre' => $id, 
													'archivo_publico' => $estado_publico,
													'link1' => $links[0],
													'link2' => $links[1],
													'link3' => $links[2]));
					
					
					}
				
				post_success();
			}


post_error();

?>