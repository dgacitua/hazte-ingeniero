<?php 

		if(Auth::user()) { // esta loggeado
			
		
		$GLOBALS['admin_name'] = Auth::user()->username;
		$GLOBALS['logged'] = 1;
		
		// permisos
		
		$GLOBALS['crear' ] = 1; 
		$GLOBALS['editar'] = 1;
		$GLOBALS['borrar'] = 1; 
		
	
		
			} else { // no esta loggeado
				
				
				$GLOBALS['admin_name'] = false;
				$GLOBALS['logged'] = false;
				
				// permisos
				
				$GLOBALS['crear' ] = false; 
				$GLOBALS['editar'] = false;
				$GLOBALS['borrar'] = false; 
				
				}

// Descripcion meta pagina web

$GLOBALS['descripcion_meta'] = "Portal de apuntes para estudiantes del Módulo Básico de Ingeniería de la USACH";
$GLOBALS['autor_meta'] = "Team Clippy USACH";



// maxlengths

$GLOBALS['maxlength_ambas_nombre'] = 64;
$GLOBALS['maxlength_ambas_desc'] = 256;
$GLOBALS['maxlength_archivo_link'] = 256;
$GLOBALS['maxlength_carpeta_slug'] = 64;



?>