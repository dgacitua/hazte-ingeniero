<?php

session_start();

require_once(app_path().'/library/mini_config.php');	
require_once(app_path().'/library/string_functions.php');
require_once(app_path().'/views/layout/layout_functions.php');


//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////


			// PLANTILLA




/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/** ------------------------------------------
 *  Route model binding
 *  ------------------------------------------
 */
Route::model('user', 'User');
Route::model('comment', 'Comment');
Route::model('post', 'Post');
Route::model('role', 'Role');

/** ------------------------------------------
 *  Route constraint patterns
 *  ------------------------------------------
 */
Route::pattern('comment', '[0-9]+');
Route::pattern('post', '[0-9]+');
Route::pattern('user', '[0-9]+');
Route::pattern('role', '[0-9]+');
Route::pattern('token', '[0-9a-z]+');

/** ------------------------------------------
 *  Admin Routes
 *  ------------------------------------------
 */
Route::group(array('prefix' => 'admin', 'before' => 'auth'), function()
{

    # Comment Management
    Route::get('comments/{comment}/edit', 'AdminCommentsController@getEdit');
    Route::post('comments/{comment}/edit', 'AdminCommentsController@postEdit');
    Route::get('comments/{comment}/delete', 'AdminCommentsController@getDelete');
    Route::post('comments/{comment}/delete', 'AdminCommentsController@postDelete');
    Route::controller('comments', 'AdminCommentsController');

    # Blog Management
    Route::get('blogs/{post}/show', 'AdminBlogsController@getShow');
    Route::get('blogs/{post}/edit', 'AdminBlogsController@getEdit');
    Route::post('blogs/{post}/edit', 'AdminBlogsController@postEdit');
    Route::get('blogs/{post}/delete', 'AdminBlogsController@getDelete');
    Route::post('blogs/{post}/delete', 'AdminBlogsController@postDelete');
    Route::controller('blogs', 'AdminBlogsController');

    # User Management
    Route::get('users/{user}/show', 'AdminUsersController@getShow');
    Route::get('users/{user}/edit', 'AdminUsersController@getEdit');
    Route::post('users/{user}/edit', 'AdminUsersController@postEdit');
    Route::get('users/{user}/delete', 'AdminUsersController@getDelete');
    Route::post('users/{user}/delete', 'AdminUsersController@postDelete');
    Route::controller('users', 'AdminUsersController');

    # User Role Management
    Route::get('roles/{role}/show', 'AdminRolesController@getShow');
    Route::get('roles/{role}/edit', 'AdminRolesController@getEdit');
    Route::post('roles/{role}/edit', 'AdminRolesController@postEdit');
    Route::get('roles/{role}/delete', 'AdminRolesController@getDelete');
    Route::post('roles/{role}/delete', 'AdminRolesController@postDelete');
    Route::controller('roles', 'AdminRolesController');

    # Admin Dashboard
    Route::controller('/', 'AdminDashboardController');
});


/** ------------------------------------------
 *  Frontend Routes
 *  ------------------------------------------
 */

// User reset routes
Route::get('user/reset/{token}', 'UserController@getReset');
// User password reset
Route::post('user/reset/{token}', 'UserController@postReset');
//:: User Account Routes ::
Route::post('user/{user}/edit', 'UserController@postEdit');

//:: User Account Routes ::
Route::post('user/login', 'UserController@postLogin');

# User RESTful Routes (Login, Logout, Register, etc)
Route::controller('user', 'UserController');

//:: Application Routes ::

# Filter for detect language
//Route::when('contact-us','detectLang');

# Contact Us Static Page
/*Route::get('contact-us', function()
{
    // Return about us page
    return View::make('site/contact-us');
});*/

# Posts - Second to last set, match slug
//Route::get('{postSlug}', 'BlogController@getView');
//Route::post('{postSlug}', 'BlogController@postView');









//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////


			// FIN PLANTILLA























//++++++
//++++++ AJAX
//++++++
//++++++

/*

Route::post('modificar-pagina-ajax.php', function() {
	
if(isset($_POST["action"]) && $_POST["action"]!="" && $GLOBALS["logged"]) { // debe haber una accion a ejecutar con AJAX y estar loggeado
	
		
		require_once(app_path().'/library/ajax_proceso.php');			
			
	}
});

*/


//++++++
//++++++ Sitios personalizados
//++++++
//++++++


Route::get('acerca', function() { return View::make('acerca');	});
// agregar otros sitios aca usando mismo sintaxis


//++++++
//++++++ Entregar URL al controllador de carpetas, para asi procesar, y saber que archivos y subcarpetas mostrar
//++++++
//++++++
//++++++ NOTESE: La expresion regex no considera el signo #, ya que este se ocupa para indicar a que parte de la pagina ir.
//++++++ no es parte de la URL en si (ejemplo: enciclopedia.com/gato.html#colmillos)



Route::pattern('urlslug', '[a-zA-Z0-9-/]+'); // debe tener solo a-z 0-9 A-Z , guion, y slash
Route::any('/{urlslug?}', function($url_slug = null){
		
		
		if(isset($_SESSION['error_mensaje_post_data'])) {
			$GLOBALS['error_mensaje_post_data'] = $_SESSION['error_mensaje_post_data'];
			unset($_SESSION['error_mensaje_post_data']);
			} else {
				$GLOBALS['error_mensaje_post_data'] = "";
				}
		
		
		
		if(isset($_POST["action"]) && $_POST["action"]!="" && $GLOBALS["logged"]) {				
				require_once(app_path().'/library/post_data_proceso.php');			
			}
			



		return App::make("CarpetasController")->getCarpetas($url_slug);

	});


//++++++ En el MVC del explorador de carpetas, se puede arrojar un error 404 manualmente en caso de que la carpeta no existe.

//++++++
//++++++ Error 404, cuando la URL contiene un caracter que no es a-z A-Z 0-9, guion y slash, y quizas otras excepciones
//++++++
//++++++


App::missing(function($exception)
	{
		return Response::view('error', array(), 404);
	});
	
	
//++++++
//++++++ Existen otros errores pero mucho mas rebuscados, como colocar un % en la URL (sin usar alguna codificacion como %20 = espacio)
//++++++ Pero Wordpress no maneja este error elegantemente tampoco, simplemente sale un mensaje del servidor, asi que quedara asi no mas.
//++++++ (Aca termina todo el manejo de URL y de errores de URL)
//++++++

